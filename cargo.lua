Cargo = class({
	pos = nil,
	center = Vec2.new(6*2, 6*2),
	rotation = Vec2.new(0, 0),
	rotationSpeed = nil,
	value = nil,
	speed = nil
})

function Cargo:construct (pos)

	self.pos = pos
	self.rotationSpeed = Vec2.new(math.random(2000)/1000, math.random(2000)/1000)
	self.value = math.random(10)
	self.speed = Vec2( (math.random(100)/500)-0.1, (math.random(100)/500)-0.1 )
end

function Cargo:tick ()

	self.pos:add(self.speed)
	self.rotation:add(self.rotationSpeed)
end

function Cargo:render ()

	x = (self.pos.x+offset.x)-self.center.x
	y = (self.pos.y+offset.y)-self.center.y

	shaders.quad:uniform('u_color', 1.0, 1.0, 1.0, 1.0)

	local size = 12
	local zindex = -12

	-- Top, main
	matrix.rotate(self.rotation.x, 1, 0, 0)
	matrix.rotate(self.rotation.y, 0, 1, 0)
	matrix.translate(0, 0, size)
	SpriteBuffer.render(sprites.cargo, shaders.sprite.shader, x, y, zindex)
	matrix.identity()
	
	-- Front
	matrix.rotate(self.rotation.x, 1, 0, 0)
	matrix.rotate(self.rotation.y, 0, 1, 0)
	matrix.translate(0, size, 0)
	matrix.rotate(90, 1, 0, 0)
	SpriteBuffer.render(sprites.cargo, shaders.sprite.shader, x, y, zindex)
	matrix.identity()

	-- Back
	matrix.rotate(self.rotation.x, 1, 0, 0)
	matrix.rotate(self.rotation.y, 0, 1, 0)
	matrix.translate(0, -size, 0)
	matrix.rotate(90, 1, 0, 0)
	SpriteBuffer.render(sprites.cargo, shaders.sprite.shader, x, y, zindex)
	matrix.identity()
	
	-- Bottom
	matrix.rotate(self.rotation.x, 1, 0, 0)
	matrix.rotate(self.rotation.y, 0, 1, 0)
	matrix.translate(0, 0, -size)
	SpriteBuffer.render(sprites.cargo, shaders.sprite.shader, x, y, zindex)
	matrix.identity()

	-- Right
	matrix.rotate(self.rotation.x, 1, 0, 0)
	matrix.rotate(self.rotation.y, 0, 1, 0)
	matrix.translate(size, 0, 0)
	matrix.rotate(90, 0, 1, 0)
	SpriteBuffer.render(sprites.cargo, shaders.sprite.shader, x, y, zindex)
	matrix.identity()

	-- Left
	matrix.rotate(self.rotation.x, 1, 0, 0)
	matrix.rotate(self.rotation.y, 0, 1, 0)
	matrix.translate(-size, 0, 0)
	matrix.rotate(90, 0, 1, 0)
	SpriteBuffer.render(sprites.cargo, shaders.sprite.shader, x, y, zindex)
	matrix.identity()
end