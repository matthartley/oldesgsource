Entity = class({
	pos = nil
})

function Entity:construct (x, y)

	self.pos = Vec2.new(x, y)
end

function Entity:render ()

	-- video.enableTextures(false)
	-- video.color(0.0, 1.0, 1.0, 1)
	-- video.renderQuad(self.pos.x+offset.x, self.pos.y+offset.y, 64, 64)

	shader.uniform4(shaders.quad, 'u_color', 1.0, 0.0, 1.0, 1.0)
	ShapeBuffer.render(shapes.entity, shaders.quad, self.pos.x+offset.x, self.pos.y+offset.y, -2)

	--shader.use(0)
end