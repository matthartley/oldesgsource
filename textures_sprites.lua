shapes = {
	--healthLine = video.quadShapeBuffer(1, 16)
	healthLine = ShapeBuffer.new(1, 16),
	bullet = ShapeBuffer.new(2, 2),
	defaultCollider = ShapeBuffer.new(10, 10),
	entity = ShapeBuffer.new(64, 64),
	particle = ShapeBuffer.new(1, 1),
	star = ShapeBuffer.new(1, 1),
	waypoint = ShapeBuffer.new(8, 8),
	minimapBorder = ShapeBuffer.new(300+2, 200+2),
	rocket = ShapeBuffer.new(8, 16),
}

textures = {
	usf119 = video.loadTexture(ASSETS..'usf119.png'),
	usf119engine = video.loadTexture(ASSETS..'usf119engine.png'),
	usf102 = video.loadTexture(ASSETS..'usf102.png'),
	usf102engine = video.loadTexture(ASSETS..'usf102engine.png'),
	asteroids = video.loadTexture(ASSETS..'asteroidsheet.png'),
	cargo = video.loadTexture(ASSETS..'cargo.png'),
	font = video.loadTexture(ASSETS..'font.png'),
	spacestation = video.loadTexture(ASSETS..'space_station.png'),

	explosion = video.loadTexture(ASSETS..'explosion.png')
}

sprites = {
	
	-- usf119 = video.spriteBuffer(textures.usf119, 0, 0, 24, 46, 2),
	-- usf119engine = video.spriteBuffer(textures.usf119engine, 0, 0, 24, 46, 2),

	-- usf102 = video.spriteBuffer(textures.usf102, 0, 0, 42, 59, 2),
	-- usf102engine = video.spriteBuffer(textures.usf102engine, 0, 0, 42, 59, 2),

	-- asteroid1 = video.spriteBuffer(textures.asteroids, 0, 0, 120, 120, 2),
	-- cargo = video.spriteBuffer(textures.cargo, 0, 0, 12, 12, 2),

	usf119 = SpriteBuffer.new(textures.usf119, 0, 0, 24, 46, 2),
	usf119engine = SpriteBuffer.new(textures.usf119engine, 0, 0, 24, 46, 2),

	usf102 = SpriteBuffer.new(textures.usf102, 0, 0, 42, 59, 2),
	usf102engine = SpriteBuffer.new(textures.usf102engine, 0, 0, 42, 59, 2),

	asteroid1 = SpriteBuffer.new(textures.asteroids, 0, 0, 120, 120, 2),
	cargo = SpriteBuffer.new(textures.cargo, 16, 0, 12, 12, 2),

	spacestation = SpriteBuffer.new(textures.spacestation, 0, 0, 88, 88, 4),

	explosion = {
		SpriteBuffer.new(textures.explosion, 0, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*2, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*3, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*4, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*5, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*6, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*7, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*8, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*9, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*10, 0, 38, 36, 2),
		SpriteBuffer.new(textures.explosion, 48*11, 0, 38, 36, 2),
	}

}

fontsprites = {}

for i = 1, (26+26+10+2+23) do
	table.insert(fontsprites,
		SpriteBuffer.new( textures.font, math.floor((i-1)%16)*8, math.floor((i-1)/16)*8, 8, 8, 1 )
	)
end