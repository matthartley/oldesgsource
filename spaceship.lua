Spaceship = class({

	pos = nil,
	side = nil,

	center = Vec2(0, 0),
	target = Vec2(0, 0),
	angle = 0,
	enginesOn = false,

	speed = Vec2(0.0, 0.0),
	rotationSpeed = 0.0,
	speedMultiplier = 1,
	rotationSpeedMultiplier = 2,
	maxAngleSpeed = Vec2(0, 0),

	reloadTimer = 0,
	forwardGunReload = 0,
	dangerZoneSize = 300,

	maxLife = 100,
	maxShield = 100,
	life = 100,
	shield = 100,
	dead = false,

	booshSpeed = Vec2(0, 0),

	ctrlDown = false

})

function Spaceship:construct (x, y, side)

	self.pos = Vec2(x, y)
	self.side = side

	-- self.center = Vec2.new(0, 0)
	-- self.target = Vec2.new(x, y)
	-- self.angle = 0
	-- self.enginesOn = false
	
	-- self.speed = Vec2.new(0.0, 0.0)
	-- self.rotationSpeed = 0.0
	-- self.speedMultiplier = 1
	-- self.rotationSpeedMultiplier = 2
	-- self.maxAngleSpeed = Vec2.new(0, 0)
	
	-- self.reloadTimer = 0
	-- self.forwardGunReload = 0
	-- self.dangerZoneSize = 300

	-- self.maxLife = 100
	-- self.maxShield = 100
	-- self.life = self.maxLife
	-- self.shield = self.maxShield
	-- self.dead = false
end

function Spaceship:render ()
end

function Spaceship:tick_start ()

	if self.booshSpeed.x ~= 0 or self.booshSpeed.y ~= 0 then
		self.booshSpeed.x = self.booshSpeed.x * 0.9;
		self.booshSpeed.y = self.booshSpeed.y * 0.9;

		if (self.booshSpeed.x < 0.1 and self.booshSpeed.x > -0.1) or (self.booshSpeed.y < 0.1 and self.booshSpeed.y > -0.1) then
			self.booshSpeed = Vec2(0, 0)
		end
	end

	self.pos:add(self.booshSpeed)

	self.enginesOn = false
	self.rotating = false
	if self.reloadTimer > 0 then self.reloadTimer = self.reloadTimer - 1 end

	--if self.side == PLAYER then self:playerTick()
	--else self:aiTick() end
	
	
end

function Spaceship:tick_end ()

	if not self.rotating then
		if self.rotationSpeed > 0 then self.rotationSpeed = self.rotationSpeed - (0.02*self.rotationSpeedMultiplier) end
		if self.rotationSpeed < 0 then self.rotationSpeed = self.rotationSpeed + (0.02*self.rotationSpeedMultiplier) end
		if self.rotationSpeed < 0.05 and self.rotationSpeed > -0.05 then self.rotationSpeed = 0 end
	end

	self:move()
	self.angle = self.angle - (self.rotationSpeed*self.rotationSpeedMultiplier)

	-- shield regen
	if self.shield < self.maxShield then self.shield = self.shield + 0.1 end

	-- Bullet collisions
	for i, bullet in ipairs(bullets) do
		if bullet.tag ~= self.side and self.collider:collision(self.pos, bullet.collider, bullet.pos) then
			bullet.dead = true
			if self.shield <= 0 then self.life = self.life - 10
			else self.shield = self.shield - 10 end
		end
	end
end

function Spaceship:aiTick ()

	
end

function Spaceship:move ()

	--self.pos.x = self.pos.x - (math.sin(math.rad(self.angle+180)) * (self.speed*self.speedMultiplier))
	--self.pos.y = self.pos.y + (math.cos(math.rad(self.angle+180)) * (self.speed*self.speedMultiplier))
	self.pos:add(self.speed)

	--if not self.enginesOn then
		self.speed.x = self.speed.x * 0.99
		self.speed.y = self.speed.y * 0.99
	--end

	if not self.enginesOn then
		if self.speed.x < 0.1 and self.speed.x > -0.1 then self.speed.x = 0 end
		if self.speed.y < 0.1 and self.speed.y > -0.1 then self.speed.y = 0 end
	end
end

function Spaceship:engines ()

	self.enginesOn = true
	--if self.speed < 3 then self.speed = self.speed + 0.05 end

	local x = -( (math.sin(math.rad(self.angle+180)) * self.speedMultiplier)/10 )
	local y = ( (math.cos(math.rad(self.angle+180)) * self.speedMultiplier)/10 )
	--self.speed.x = self.speed.x -( (math.sin(math.rad(self.angle+180)) * self.speedMultiplier)/10 )
	--self.speed.y = self.speed.y + ( (math.cos(math.rad(self.angle+180)) * self.speedMultiplier)/10 )
	local mult = (10 - self.speed:len()) / 10
	self.speed.x = self.speed.x + (x * mult)
	self.speed.y = self.speed.y + (y * mult)
	

	

	-- self.maxAngleSpeed.x = -(math.sin(math.rad(self.angle+180))*3)
	-- self.maxAngleSpeed.y = math.cos(math.rad(self.angle+180))*3

	-- if self.maxAngleSpeed.x > 0 then
	-- 	if self.speed.x > self.maxAngleSpeed.x then self.speed.x = self.maxAngleSpeed.x end
	-- end
	-- if self.maxAngleSpeed.x < 0 then
	-- 	if self.speed.x < self.maxAngleSpeed.x then self.speed.x = self.maxAngleSpeed.x end
	-- end
	-- if self.maxAngleSpeed.y > 0 then
	-- 	if self.speed.y > self.maxAngleSpeed.y then self.speed.y = self.maxAngleSpeed.y end
	-- end
	-- if self.maxAngleSpeed.y < 0 then
	-- 	if self.speed.y < self.maxAngleSpeed.y then self.speed.y = self.maxAngleSpeed.y end
	-- end
end

function Spaceship:sideThrusters (dir)

	-- self.angle = self.angle - dir*(self.rotationSpeed*self.rotationSpeedMultiplier)
	-- self.sideThrustersOn = true
	-- if self.rotationSpeed < 1 and self.rotationSpeed > -1 then self.rotationSpeed = self.rotationSpeed + dir*(0.02*self.rotationSpeedMultiplier) end
end

function Spaceship:rotateLeft ()

	self.rotating = true
	if self.rotationSpeed < 1 then self.rotationSpeed = self.rotationSpeed + (0.02*self.rotationSpeedMultiplier) end
end

function Spaceship:rotateRight ()

	self.rotating = true
	if self.rotationSpeed > -1 then self.rotationSpeed = self.rotationSpeed - (0.02*self.rotationSpeedMultiplier) end
end

function Spaceship:shoot (otherShipPosition)

	self.reloadTimer = 20 -- change
	local normal = self.pos:difference(otherShipPosition):normal()
	normal.x = normal.x + (math.random()*0.2 - 0.1)
	normal.y = normal.y + (math.random()*0.2 - 0.1)
	table.insert(bullets, Bullet.new( self.pos.x, self.pos.y, normal, 15, self.side ))
end

function Spaceship:shootForwardGuns ()

	for i, gun in ipairs(self.forwardGuns) do
		normal = Vec2.new(-math.sin(math.rad(self.angle+180)), math.cos(math.rad(self.angle+180)))
		table.insert(bullets, Bullet.new( self.pos.x+((gun.x * -normal.y)+(gun.y * normal.x)), self.pos.y+((gun.y * normal.y)+(gun.x * normal.x)), normal, 15, self.side ))
	end
end

function Spaceship:renderDangerZone ()

	-- if self.side==ENEMY then
	-- 	video.enableTextures(false)
	-- 	video.color(1, 0, 0, 0.1)
	-- 	video.renderCircle(self.pos.x+offset.x, self.pos.y+offset.y, self.dangerZoneSize, 1)
	-- end
end

function Spaceship:testShipCollision (othership)

	local dist = self.pos:distance(othership.pos)
	if dist < ((self.center.y*0.8)+(othership.center.y*0.8)) then
		self.life = self.life - (self.speed:len()+othership.speed:len())
		local boosh = self.pos:difference(othership.pos):normal()
		self.speed:add( boosh:multipied(1):invert() )

		for k = 1, 1 do
			table.insert(particles, Particle.new( self.pos:added( boosh:multipied((self.pos:distance(othership.pos)/2)) ) ));
		end
	end
end