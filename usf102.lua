Usf102 = extends(Spaceship, {

	center = Vec2(21*2, 32*2),
	rotationSpeedMultiplier = 0.7,
	collider = Collider( Vec2(-48, -48), Vec2(96, 96) )

})

function Usf102:construct (x, y, side)

	self.pos = Vec2(x, y)
	self.side = side
end

function Usf102:render ()

	self:renderDangerZone()

	x = (self.pos.x+offset.x)
	y = (self.pos.y+offset.y)

	SpriteBuffer.render(sprites.usf102, shaders.sprite.shader, x, y, -1, self.angle)
	if self.enginesOn then
		SpriteBuffer.render(sprites.usf102engine, shaders.sprite.shader, x, y, -1, self.angle)
	end

	--shader.use(0)

	--self.collider:render( Vec2.new(self.pos.x+offset.x, self.pos.y+offset.y) )
end