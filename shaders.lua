function loadFile (path)

	local str = ''
	local file = io.open(path)
	-- if file then
	-- 	for line in file:lines() do
	-- 		str = str .. line .. '\n'
	-- 	end
	-- end
	--print(file:read('*a'))

	return file:read('*a')
end

function compileShader ()


end

shaders = {
	-- quad = shader.compile( loadFile(SRC..'shader/quad.vert'), loadFile(SRC..'shader/quad.frag') ),
	-- sprite = shader.compile( loadFile(SRC..'shader/sprite.vert'), loadFile(SRC..'shader/sprite.frag') ),
	-- stars = shader.compile( loadFile(SRC..'shader/stars.vert'), loadFile(SRC..'shader/stars.frag') ),
	quad = shader.from_file( SRC..'shader/quad.frag' ),
	sprite = shader.from_file( SRC..'shader/sprite.frag' ),
	stars = shader.from_file( SRC..'shader/stars.frag' )
}