Rocket = class({

	pos = nil,
	angle = 0,
	speed = 1

})

function Rocket:construct (pos, angle)

	self.pos = pos
	self.angle = angle
end

function Rocket:render ()

	shaders.quad:uniform('u_color', 1, 1, 1, 1)
	ShapeBuffer.render(shapes.rocket, shaders.quad.shader, self.pos.x+offset.x, self.pos.y+offset.y, 0, self.angle)
end

function Rocket:tick ()

	if self.speed < 10 then
		self.speed = self.speed + 0.2
	end

	self.pos.x = self.pos.x + ( -math.sin(math.rad(self.angle+180)) * self.speed)
	self.pos.y = self.pos.y + ( math.cos(math.rad(self.angle+180)) * self.speed)
end