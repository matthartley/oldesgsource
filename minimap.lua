Minimap = class({

})

function Minimap:construct ()

	esg.initMinimap()
end

function Minimap:render ()

	esg.beginRenderMinimap()
		--font:render('player x '..player.ship.pos.x, 10, 10)

		for i, location in ipairs(locations) do
			matrix.scale(0.5, 0.5, 1)
			shaders.quad:uniform('u_color', 0.0, 1.0, 0.3, 1.0)
			ShapeBuffer.render(shapes.waypoint, shaders.quad.shader, (player.ship.pos.x+offset.x)/50 + 150, (player.ship.pos.y+offset.y)/50 + 100, 0)
			shaders.quad:uniform('u_color', 1.0, 1.0, 1.0, 1.0)
			ShapeBuffer.render(shapes.waypoint, shaders.quad.shader, (location.pos.x+offset.x)/50 + 150, (location.pos.y+offset.y)/50 + 100, 0)
			matrix.identity()
			--fontBig:render(location.name, ((location.pos.x/100)+offset.x+8) - ((#location.name*fontBig.size)/2), ((location.pos.y/100)+offset.y)-16, 16)
		end

	shader.use({shader=0})
	esg.endRenderMinimap()
end