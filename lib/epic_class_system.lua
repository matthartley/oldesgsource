-- Epic class system

function copy_funcs (copy, printinfo)

	if printinfo then print('Printing function copy info...') end

	local table = {}

	for i, v in pairs(copy) do
		if type(v) == 'function' then
			table[i] = v
			if printinfo then print(i) end
		end
	end

	return table
end

function class ()

	local classTable = {}

	classTable.new = function ( ... )

		-- local instance = instance(classTable)
		-- instance:construct( ... )
		-- return instance
		local instance = copy_funcs(classTable) --setmetatable({}, { __index = classTable })
		instance:construct( ... )
		return instance
	end

	classTable = setmetatable(classTable, { __call = function ( t, ... ) return classTable.new( ... ) end })

	return classTable
end

function extends (parent, printinfo)

	--local classTable = setmetatable({}, { __index = parent })
	local classTable = {}

	classTable.new = function ( ... )

		local instance = setmetatable(copy_funcs(classTable, printinfo), { __index = parent.new() })
		instance:construct( ... )
		return instance
	end

	classTable = setmetatable(classTable, { __call = function ( t, ... ) return classTable.new( ... ) end })

	classTable.super = parent
	return classTable
end

-- function instance(classTable)
	
-- 	local instanceTable
-- 	if not classTable.super then 
-- 		instanceTable = setmetatable({}, { __index = classTable })
-- 	else
-- 		instanceTable = setmetatable(classTable.super.new(), { __index = classTable })
-- 	end
-- 	return instanceTable
-- end



-- Animal = Epic.class()

-- function Animal:construct (name)
	
-- 	self.name = name
-- 	self.className = 'Animal'
-- end

-- function Animal:printClassName ()
	
-- 	print(self.className, self.name)
-- end



-- Dog = Epic.inherit(Animal)

-- function Dog:construct (name, type)
	
-- 	self.name = name
-- 	self.type = type
-- end

-- function Dog:printType ()
	
-- 	print(self.name..' type = '..self.type)
-- end

-- Cat = Epic.inherit(Animal)

-- function Cat:construct (name, sleepiness)
	
-- 	self.name = name
-- 	self.sleepiness = sleepiness
-- end

-- function Cat:sleep ()
	
-- 	local str = self.name..' '
-- 	for i = 0, self.sleepiness do
-- 		str = str..'z'
-- 	end

-- 	print(str)
-- end



-- Oreo = Epic.inherit(Cat)

-- function Oreo:construct (derpiness)
	
-- 	self.name = 'Oreo'
-- 	self.sleepiness = 30
-- 	self.derpiness = derpiness
-- end

-- function Oreo:info ()
	
-- 	print(self.name..' is '..self.derpiness..' today')
-- 	self:sleep()
-- end



-- oreo = Oreo.new('very derpy')
-- oreo:info()