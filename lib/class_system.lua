-- Another new class system

indent = ''
function print_table (table)

	print(indent..'table')
	print(indent..'{')
	for i, v in pairs(table) do
		if type(v) == 'table' then
			indent = indent .. '    '
			print(indent..i..':')
			print_table(v)
			indent = string.sub(indent, 1, #indent-4)
		else
			print(indent..'', i .. ' = ' .. (type(v) == 'function' and 'function' or (type(v) == 'boolean' and 'boolean' or v)))
		end
	end
	print(indent..'}')
end

function copy (table, copy_table)

	--print(debug.traceback())

	for i, v in pairs(copy_table) do
		if type(v) == 'table' then
			table[i] = {}
			copy(table[i], v)
		else
			table[i] = v
		end
	end
end

function class (members)

	local class = {}
	copy(class, members)

	class.new = function ( ... )

		local instance = {}
		copy(instance, class)
		instance:construct( ... )
		return instance
	end

	class = setmetatable(class, { __call = function ( t, ... ) return class.new( ... ) end })

	return class
end

function extends (parent, members)

	local class = {}
	copy(class, parent)
	copy(class, members)

	class.new = function ( ... )

		local instance = {}
		copy(instance, class)
		instance:construct( ... )
		return instance
	end

	class = setmetatable(class, { __call = function ( t, ... ) return class.new( ... ) end })

	return class
end





-- Cat = class({
-- 	head = {
-- 		eyes = {
-- 			'eye1',
-- 			'eye2'
-- 		},
-- 		ears = {
-- 			'ear1',
-- 			'ear2'
-- 		}
-- 	}
-- })

-- Cat.x = 0
-- Cat.y = 0
-- Cat.legs = 4
-- Cat.fur = true
-- Cat.speed = 10

-- function Cat:construct (speed)

-- 	self.speed = speed
-- end

-- function Cat:move ()

-- 	self.x = self.x + self.speed
-- 	self.y = self.y + self.speed
-- end

-- Kitten = extends(Cat, {
-- 	fur = 'very soft',
-- 	speed = 5
-- })

-- function Kitten:construct ()

-- 	self.description = 'Kitten is very lovely and very soft'
-- end

-- function Kitten:play ()

-- 	print('Kitten likes to play with string and balls of wool')
-- end

-- print_table(Cat)





