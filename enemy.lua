Enemy = class({

	ship = nil

})

function Enemy:construct (spaceship_type, x, y)

	self.ship = spaceship_type(x, y, ENEMY)
end

function Enemy:render ()

	if not self.ship.dead then
		self.ship:render()
	end
end

function Enemy:tick ()

	self.ship:tick_start()

	xDiff = self.ship.target.x - self.ship.pos.x
	yDiff = self.ship.target.y - self.ship.pos.y
	xDist = xDiff
	yDist = yDiff
	maxdist = 100
	dist = math.sqrt( (xDist*xDist) + (yDist*yDist) )
	angle = math.deg(math.atan2( -xDiff, yDiff )) + 180

	if dist > maxdist then
		a = 0
		if self.ship.angle < angle then a = 1 end
		if self.ship.angle > angle then a = -1 end
		if self.ship.angle+180 < angle then a = -1 end
		if self.ship.angle-180 > angle then a = 1 end

		if self.ship.angle < angle + 2 and self.ship.angle > angle - 2 then
			self.ship.angle = angle
			self.ship:engines()
		else
			if a == 1 then self.ship:rotateRight() end
			if a == -1 then self.ship:rotateLeft() end
		end

		if self.ship.angle >= 360 then self.ship.angle = self.ship.angle - 360 end
		if self.ship.angle < 0 then self.ship.angle = self.ship.angle + 360 end
	end

	-- Follow orders
	if self.ship.orders then
		if self.ship.pos:distance(self.ship.target) < 100 then
			self.ship.orders.commandIndex = self.ship.orders.commandIndex + 1
			if self.ship.orders.commandIndex > self.ship.orders.noOfCommands then self.ship.orders.commandIndex = 1 end
			self.ship.target = self.ship.orders.commands[self.ship.orders.commandIndex]
		end
	end

	-- shoot
	if not player.ship.dead then
		if self.ship.side ~= player.ship.side and self.ship.pos:distance(player.ship.pos) < 400 then
			if self.ship.reloadTimer == 0 then self.ship:shoot(player.ship.pos) end
		end
	end

	self.ship:tick_end()
end