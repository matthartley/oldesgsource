Star = extends(Entity, {

	brightness = nil,
	r = nil,
	g = nil,
	b = nil

})

function Star:construct (pos, brightness)

	self.pos = pos
	self.brightness = brightness
	self.r = math.random()
	self.g = math.random()
	self.b = math.random()
end

function Star:render (x, y)

	-- video.enableTextures(false)
	-- video.color((self.r*0.2)+0.8, (self.g*0.2)+0.8, (self.b*0.2)+0.8, self.brightness)
	-- video.renderQuad(x+self.pos.x, y+self.pos.y, 1, 1)

	shader.uniform4(shaders.quad, 'u_color', (self.r*0.2)+0.8, (self.g*0.2)+0.8, (self.b*0.2)+0.8, self.brightness)
	ShapeBuffer.render(shapes.star, shaders.quad, x+self.pos.x, y+self.pos.y, -10)

	--shader.use(0)
end