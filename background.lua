dofile(SRC..'star.lua')

Background = class({
--	stars = {}
})

-- function Background:construct ()

-- 	--for i = 1, 9000 do
-- 	for i = 1, 1000 do

-- 		table.insert(self.stars,
-- 			Star.new( Vec2.new( math.random(app.width*3)-app.width, math.random(app.height*3)-app.height ), math.random() )
-- 		)
-- 	end
-- end

-- function Background:render ()
	
-- 	local x = player.ship.pos.x/10
-- 	local y = player.ship.pos.y/10

-- 	for i, star in ipairs(self.stars) do

-- 		star:render(-x, -y)
-- 	end
-- end

function Background:construct ()

	esg.createStarMap(10000, app.width, app.height, 0, 0)
end

function Background:render ()

	-- x = (player.ship.pos.x)/10
	-- y = (player.ship.pos.y)/10
	x = (offset.x+(app.width/2))/10
	y = (offset.y+(app.height/2))/10

	shaders.stars:uniform('u_color', 0.5, 0.5, 0.5, 1.0)
	esg.renderStarMap(shaders.stars.shader, x, y, -10)
end