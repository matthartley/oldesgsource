Particle = extends(Entity, {
	pos = nil,
	speed = nil,
	life = nil,
	color = nil,
	size = nil
})

function Particle:construct (pos)

	self.pos = pos
	self.speed = Vec2.new( (math.random()*2)-1, (math.random()*2)-1 )
	self.life = math.random(500)
	self.color = (math.random()*0.4)-0.2
	self.size = Vec2.new( math.random(3), math.random(3) )
end

function Particle:render ()

	-- video.enableTextures(false)
	-- video.color(0.5+self.color, 0.5+self.color, 0.5+self.color, self.life/100)
	-- video.renderQuad(self.pos.x+offset.x, self.pos.y+offset.y, self.size.x, self.size.y)

	matrix.scale(math.random(2), math.random(2), 1)
	shaders.quad:uniform('u_color', 0.5+self.color, 0.5+self.color, 0.5+self.color, self.life/100)
	ShapeBuffer.render(shapes.particle, shaders.quad.shader, self.pos.x+offset.x, self.pos.y+offset.y, 0)
	matrix.identity()

	--shader.use(0)
end

function Particle:tick ()

	self.pos.x = self.pos.x + self.speed.x
	self.pos.y = self.pos.y + self.speed.y
	if self.life > 0 then self.life = self.life - 1 end
end