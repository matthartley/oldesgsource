Asteroid = class({
	pos = nil,
	center = Vec2.new(45*2, 50*2),
	numPoints = 0.2 + (math.random()*0.1),
	size = math.random(),
	speed = Vec2.new(-1+math.random()*2, -1+math.random()*2)
})

function Asteroid:construct (pos)

	self.pos = pos
	-- self.center = Vec2.new(45*2, 50*2)

	-- self.numPoints = 0.2 + (math.random()*0.1)
	-- self.size = math.random()

	-- self.speed = Vec2.new(-1+math.random()*2, -1+math.random()*2)
end

function Asteroid:tick ()

	self.pos:add(self.speed)
	-- self.pos.x = self.pos.x + self.speed.x
	-- self.pos.y = self.pos.y + self.speed.y
end

function Asteroid:render ()

	-- video.enableTextures(false)
	-- video.color(124/255, 99/255, 82/255, 1)
	-- video.renderCircle(self.pos.x+offset.x, self.pos.y+offset.y, 100, self.numPoints)

	-- x = (self.pos.x+offset.x)-self.center.x
	-- y = (self.pos.y+offset.y)-self.center.y

	-- video.color(1, 1, 1, 1)
	-- video.loadIdentity()
	-- video.translate(x, y)
	-- sprites.asteroid1:render(0, 0, 2)
	-- video.loadIdentity()
end