#version 120

varying vec4 v_texcoord;

uniform vec4 u_color;
uniform sampler2D u_sampler;

void main () {

	// vec2 center = vec2(1280/2, 720/2);
	// float light = (500 - distance(center, gl_FragCoord.xy)) / 500;
	// gl_FragColor = texture2D(u_sampler, v_texcoord.xy) * vec4(light, light, light, 1);
	gl_FragColor = texture2D(u_sampler, v_texcoord.xy);
}