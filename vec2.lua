Vec2 = class({

	x = nil,
	y = nil

})

function Vec2:construct (x, y)

	self.x = x
	self.y = y
end

function Vec2:len ()

	return math.sqrt(self.x^2 + self.y^2)
end

function Vec2:distance (vector)

	--print(debug.traceback())
	local asd = math.sqrt( 
		( vector.x - self.x )^2 + 
		( vector.y - self.y )^2 )
	return asd
end
function Vec2.dist (vector1, vector2)

	return math.sqrt( (vector1.x-vector2.x)^2 + (vector1.y-vector2.y)^2 )
end

function Vec2:print ()

	print( 'x : '..self.x, 'y : '..self.y )
end

function Vec2:add (vec)

	self.x = self.x + vec.x
	self.y = self.y + vec.y
end

function Vec2:sub (vec)

	self.x = self.x - vec.x
	self.y = self.y - vec.y
end

function Vec2:decrease (num)

	if self.x > 0 then self.x = self.x - num end
	if self.y > 0 then self.y = self.y - num end
end

-- RETURNS NEW VECTOR

function Vec2:difference (vec)

	return Vec2.new(vec.x-self.x, vec.y-self.y)
end

function Vec2:normal ()

	return Vec2.new(self.x/self:len(), self.y/self:len())
end

function Vec2:multipied (mult)

	return Vec2.new(self.x*mult, self.y*mult)
end

function Vec2:invert ()

	return Vec2(-self.x, -self.y)
end

function Vec2:added (vec)

	return Vec2(self.x+vec.x, self.y+vec.y)
end