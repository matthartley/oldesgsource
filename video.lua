-- extra video functionality

function video3.rotateAround (centerx, centery, rotation)

	video3.translate(centerx, centery, 0)
	video3.rotate(rotation)
	video3.translate(-centerx, -centery, 0)
end