Usf119 = extends(Spaceship, {

	center = Vec2(12*2, 24*2),
	collider = Collider( Vec2(-32, -32), Vec2(64, 64) ),
	maxLife = 200,
	life = 200,
	maxShield = 500,
	shield = 500,

	forwardGuns = {
		Vec2(10*2, -2*2),
		Vec2(-10*2, -2*2)
	}

})

function Usf119:construct (x, y, side)

	self.pos = Vec2(x, y)
	self.side = side
end

function Usf119:render ()

	-- self:renderDangerZone()

	-- video.enableTextures(false)
	-- video.color(80/255, 200/255, 1, 0.5)
	-- video.renderCircle(self.pos.x+offset.x, self.pos.y+offset.y, 50, 1)

	x = (self.pos.x+offset.x)
	y = (self.pos.y+offset.y)

	SpriteBuffer.render(sprites.usf119, shaders.sprite.shader, x, y, -1, self.angle)
	if self.enginesOn then
		SpriteBuffer.render(sprites.usf119engine, shaders.sprite.shader, x, y, -1, self.angle)
	end
	
	--shader.use(0)

	-- sprites.usf119:renderz(0, 0, 2, -1)
	-- if self.enginesOn then sprites.usf119engine:renderz(0, 0, 2, -1) end

	-- if self.shield > 0 then
	-- 	local shieldAlpha = (0.3*(self.shield/self.maxShield))
	-- 	video.enableTextures(false)
	-- 	video.color(0.0, 0.7, 1.0, 0.1 + shieldAlpha)
	-- 	video.renderElipse(self.center.x, self.center.y, 30, 50, 1)
	-- end

	--self.collider:render( Vec2.new(self.pos.x+offset.x, self.pos.y+offset.y) )
end