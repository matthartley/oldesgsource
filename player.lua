Player = class({

	ship = nil,
	cargo = 0

})

function Player:construct ()

	self.ship = Usf119(0, 0, PLAYER)
end

function Player:render ()

	if not self.ship.dead then
		self.ship:render()
	end
end

function Player:tick ()

	self.ship:tick_start()

	if not self.ship.dead then

		if self.ship.forwardGunReload > 0 then self.ship.forwardGunReload = self.ship.forwardGunReload - 1 end
		if key[keyCode.space] and self.ship.forwardGunReload == 0 then
			self.ship:shootForwardGuns()
			self.ship.forwardGunReload = 20 -- change
		end
		if key[keyCode.lctrl] and not self.ship.ctrlDown then
			table.insert(rockets, Rocket(Vec2(self.ship.pos.x+(-math.sin(math.rad(self.ship.angle+180))*self.ship.center.y), self.ship.pos.y+(math.cos(math.rad(self.ship.angle+180))*self.ship.center.y)), self.ship.angle))
			self.ship.ctrlDown = true
		end
		if key[keyCode.left] or key[keyCode.a] then self.ship:rotateLeft() end
		if key[keyCode.right] or key[keyCode.d] then self.ship:rotateRight() end
		if key[keyCode.up] or key[keyCode.w] then self.ship:engines() end
	end

	self.ship:tick_end()

	for i, c in ipairs(cargos) do
		--print_table(c.pos)
		if Vec2.dist(self.ship.pos, c.pos) < 64 then
			self.cargo = self.cargo + c.value
			table.remove(cargos, i)
		end
	end

	for i, othership in ipairs(spaceships) do
		self.ship:testShipCollision(othership.ship)
	end
end

-- function Player:dead ()

-- 	if self.ship.dead then
-- 		return true
-- 	else
-- 		return false
-- 	end
-- end