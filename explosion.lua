Explosion = class({

	pos = nil,
	animationFrame = 0,
	animationTimer = 0,
	done = false

})

function Explosion:construct (pos)

	self.pos = pos
end

function Explosion:render ()

	if not done and self.animationFrame < 12 then
		x = (self.pos.x+offset.x)
		y = (self.pos.y+offset.y)

		SpriteBuffer.render(sprites.explosion[self.animationFrame+1], shaders.sprite.shader, x, y, -1, 0)
	end
end

function Explosion:tick ()

	if not done then
		self.animationTimer = self.animationTimer + 1
		if self.animationTimer > 3 then
			self.animationTimer = 0
			self.animationFrame = self.animationFrame + 1
		end
		if self.animationFrame > 11 then
			self.done = true
		end
	end
end