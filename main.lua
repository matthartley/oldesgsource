-- print(_VERSION)

collectgarbage('setstepmul', 200)
collectgarbage('setpause', 105)

app = {
	width = 1280,
	height = 720,
	-- width = 720,
	-- height = 480,
	scale = 1,
	fullscreen = false,
	title = 'Epic Space Game',
	limitframes = true,

	fps = 0,
	tps = 0,
	memory = 0
}

SRC = 'esgsource/'
ASSETS = 'assets/'

dofile(SRC..'game.lua')

function keyDown (code)

	key[code] = true

	-- if code == keyCode.space then spaceship:shootForwardGuns() end
	if code == keyCode.one then
		table.insert(cargos, Cargo.new(Vec2( (math.random()*1000)-500, (math.random()*1000)-500 )))
	end
	if code == keyCode.two then
		enemy = math.random(2)==2 and Enemy(Usf102, 0, 0) or Enemy(Usf119, 0, 0)
		enemy.ship.pos = randLocNearSpawn()
		enemy.ship.orders = Orders({
			locations[2].pos,
			locations[3].pos
		})
		table.insert(spaceships, enemy)
	end
end

function keyUp (code)

	key[code] = false

	if code == keyCode.lctrl then
		player.ship.ctrlDown = false
	end
end

function mouseDown (button)

	-- if button == 0 then
	-- 	spaceship.target.x = input.mousex() - offset.x
	-- 	spaceship.target.y = input.mousey() - offset.y
	-- end
end

function secondstep ()

	--print(debug.traceback())

	app.fps = engine.frames()
	app.tps = engine.ticks()
	app.memory = collectgarbage('count')

	-- local f = engine.frames()
	-- local t = engine.ticks()
	-- print('f' .. f .. ' t' .. t .. '    ' .. 'memory ' .. collectgarbage('count'))
	-- print(spaceship.speed.x, spaceship.speed.y, spaceship.speed:len())
	-- print('maxAngleSpeed', spaceship.maxAngleSpeed.x, spaceship.maxAngleSpeed.y)
end