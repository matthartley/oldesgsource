SpaceStation = class({
	pos = Vec2(0, 0),
	name = ''
})

function SpaceStation:construct (name, pos)

	self.name = name
	self.pos = pos
end

function SpaceStation:render ()

	local x = (self.pos.x+offset.x)
	local y = (self.pos.y+offset.y)

	SpriteBuffer.render(sprites.spacestation, shaders.sprite.shader, x, y, -1)
end