Bullet = extends(Entity, {
	direction = nil,
	speed = nil,
	tag = nil,
	dead = false,
	collider = Collider.new( Vec2.new(-2, -2), Vec2.new(4, 4) ),
	life = 120
})

function Bullet:construct (x, y, direction, speed, tag)

	self.pos = Vec2(x, y)
	self.direction = direction
	self.speed = speed
	self.tag = tag
end

function Bullet:render ()

	-- video.enableTextures(false)
	-- video.color(1, 1, 0, 1)
	-- video.renderQuad(self.pos.x+offset.x, self.pos.y+offset.y, 2, 2)

	shaders.quad:uniform('u_color', 1.0, 1.0, 0, 1.0)
	ShapeBuffer.render(shapes.bullet, shaders.quad.shader, self.pos.x+offset.x, self.pos.y+offset.y, 0)

	--shader.use(0)

	--self.collider:render( Vec2.new(self.pos.x+offset.x, self.pos.y+offset.y) )
end

function Bullet:tick ()

	self.pos:add( self.direction:multipied(self.speed) )
	self.life = self.life - 1
	if self.life < 1 then self.dead = true end
end