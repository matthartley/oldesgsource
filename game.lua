dofile(SRC..'lib/sys.lua')
dofile(SRC..'lib/class_system.lua')

dofile(SRC..'video.lua')
dofile(SRC..'enums.lua')
dofile(SRC..'vec2.lua')
dofile(SRC..'sprite.lua')
dofile(SRC..'collider.lua')
dofile(SRC..'entity.lua')
dofile(SRC..'spaceship.lua')
dofile(SRC..'usf119.lua')
dofile(SRC..'usf102.lua')
dofile(SRC..'orders.lua')
dofile(SRC..'bullet.lua')
dofile(SRC..'particle.lua')
dofile(SRC..'background.lua')
dofile(SRC..'asteroid.lua')
dofile(SRC..'cargo.lua')
dofile(SRC..'font.lua')
dofile(SRC..'player.lua')
dofile(SRC..'enemy.lua')
dofile(SRC..'minimap.lua')
dofile(SRC..'space_station.lua')
dofile(SRC..'explosion.lua')
dofile(SRC..'rocket.lua')

math.randomseed(os.time())

font = Font.new(12)
fontBig = Font.new(16)

-- textures and sprites used to be here
dofile(SRC..'shaders.lua')
dofile(SRC..'textures_sprites.lua')

function randLocNearSpawn ()

	return Vec2( (math.random()*1000)-500, (math.random()*1000)-500 )
end

locations = {
	{
		name = 'Home',
		pos = Vec2(0, 0)
	},
	{
		name = 'Waypoint 1',
		pos = Vec2(-1000, -400)
	},
	{
		name = 'Waypoint 2',
		pos = Vec2(1000, 400)
	},
	SpaceStation('Federation Station Alpha', Vec2(1500, -1500))
}

offset = Vec2.new(0, 0)
entity = Entity.new(-300, -300)

background = Background.new()
player = Player()
--spaceship = Usf119(0, 0, PLAYER)
--print_table(spaceship)
-- for key, value in pairs(spaceship) do
-- 	print(key, value)
-- end
-- for key, value in pairs(spaceship.pos.x) do
-- 	print(key, value)
-- end

bullets = {}
bulletCount = 0
particles = {}
particleCount = 0
rockets = {}
rocketCount = 0
cargos = {
	Cargo(Vec2(-200, -200))
}
effects = {}

spaceships = {
	Enemy(Usf102, 200, 0),
	Enemy(Usf119, -200, 0)
}

spaceships[1].ship.orders = Orders.new({
	locations[2].pos,
	locations[3].pos
})

spaceships[2].ship.orders = Orders.new({
	locations[3].pos,
	locations[2].pos
})

minimap = Minimap()

ast = Asteroid.new(Vec2.new(300, 100))

function render () 

	video.clear()

	background:render()

	--ShapeBuffer.render(test, shaders.quad, offset.x - 200, offset.y -200, 0, 0)

	--video.enableTextures(false)
	--video.color(0.0, 1.0, 1.0, 1)
	--video.renderQuad(10, 10, 100, 100)

	-- video.enableTextures(false)
	-- video.color(0.0, 1.0, 1.0, 1)
	-- video.renderQuad(spaceship.target.x+offset.x, spaceship.target.y+offset.y, 2, 2)

	--video.color(1, 1, 1, 1)
	--video.renderSprite(usf102, 200+offset.x, 0+offset.y, 128, 128)

	--entity:render()

	player:render()
	
	for i, ship in ipairs(spaceships) do
		ship:render()
	end
	for i, bullet in ipairs(bullets) do
		bullet:render()
	end
	for i, particle in ipairs(particles) do
		particle:render()
	end
	for i, rocket in ipairs(rockets) do
		rocket:render()
	end
	for i, cargo in ipairs(cargos) do
		cargo:render()
	end
	for i, effect in ipairs(effects) do
		effect:render()
	end
	for i, location in ipairs(locations) do
		shaders.quad:uniform('u_color', 1.0, 1.0, 1.0, 1.0)
		ShapeBuffer.render(shapes.waypoint, shaders.quad.shader, location.pos.x+offset.x, location.pos.y+offset.y, 0)
		fontBig:render(location.name, (location.pos.x+offset.x+8) - ((#location.name*fontBig.size)/2), (location.pos.y+offset.y)-16, 16)
		if location.render ~= nil then
			location:render()
		end
	end

	-- video.enableTextures(false)
	-- video.color(0.0, 1.0, 1.0, 1)
	-- video.renderQuad(spaceship.x+xOriginOffset+spaceship.xCenter, spaceship.y+yOriginOffset+spaceship.yCenter, 2, 2)

	-- Life and shield bars
	--video.enableTextures(false)

	shaders.quad:uniform('u_color', 1.0, 0, 0, 1.0)
	if player.ship.life >= player.ship.maxLife/4 then shaders.quad:uniform('u_color', 1.0, 0.7, 0.0, 1.0) end
	if player.ship.life >= player.ship.maxLife/2 then shaders.quad:uniform('u_color', 0.0, 1.0, 0.3, 1.0) end

	for i = 1, (player.ship.life/player.ship.maxLife)*100 do
		ShapeBuffer.render(shapes.healthLine, shaders.quad.shader, 10+(i*3)+0.5, 10+8, 0)
	end

	shaders.quad:uniform('u_color', 1.0, 0, 0, 1.0)
	if player.ship.shield >= player.ship.maxShield/4 then shaders.quad:uniform('u_color', 1.0, 0.7, 0.0, 1.0) end
	if player.ship.shield >= player.ship.maxShield/2 then shaders.quad:uniform('u_color', 0.0, 0.7, 1.0, 1.0) end

	for i = 1, (player.ship.shield/player.ship.maxShield)*100 do
		ShapeBuffer.render(shapes.healthLine, shaders.quad.shader, 10+(i*3)+0.5, 36+8, 0)
	end

	-- Minimap
	shaders.quad:uniform('u_color', 1.0, 1.0, 1.0, 1.0)
	ShapeBuffer.render(shapes.minimapBorder, shaders.quad.shader, 302/2, app.height - (202/2), -1)
	minimap:render()

	
	-- Gui text
	str = 'Epic // Space // Game '
	font:render(str, (app.width/2)-(#str/2*font.size), font.size)

	fpsinfo = 'f ' .. app.fps .. ' t ' .. app.tps
	memoryinfo = 'memory ' .. math.floor(app.memory) .. 'kb'
	qps = 'quads per second ' .. engine.qps()
	font:render(fpsinfo, app.width - (#fpsinfo*font.size), font.size)
	font:render(memoryinfo, app.width - (#memoryinfo*font.size), font.size+font.lineHeight)
	font:render(qps, app.width - (#qps*font.size), font.size+(font.lineHeight*2))

	info = {
		'x '..simplify(player.ship.pos.x)..', y '..simplify(player.ship.pos.y),
		'speed x      '..simplify(player.ship.speed.x),
		'speed y      '..simplify(player.ship.speed.y),
		'speed normal '..simplify(player.ship.speed:len()),
		'offset '..simplify(offset.x)..', '..simplify(offset.y),
		''..#locations[1].name,
		--cgo.rotation.x .. ' ' .. cgo.rotation.y,
		--cgo.rotationSpeed.x .. ' ' .. cgo.rotationSpeed.y,
		--spaceships[0].pos.x .. ' ' .. spaceships[0].pos.y,
		--'spaceship[1] ' .. spaceships[1].pos.x .. ' ' .. spaceships[1].pos.y
		'spaceships '..table.getn(spaceships),
		'cargos     '..table.getn(cargos),
		'bullets    '..table.getn(bullets),
		'particles  '..table.getn(particles),
		'rockets    '..table.getn(rockets)
	}

	for i, v in ipairs(info) do
		font:render(v, font.size, font.size+((i-1)*font.lineHeight)+64)
	end

	cargoinfo = player.cargo..' cargo'
	font:render(cargoinfo, app.width-(#cargoinfo*font.size), app.height - font.size)
end

function tick ()

	if player.ship.dead then
		if key[keyCode.left] or key[keyCode.a] then offset:add(Vec2(10, 0)) end
		if key[keyCode.right] or key[keyCode.d] then offset:add(Vec2(-10, 0)) end
		if key[keyCode.up] or key[keyCode.w] then offset:add(Vec2(0, 10)) end
		if key[keyCode.down] or key[keyCode.s] then offset:add(Vec2(0, -10)) end
	else
		offset.x = -( (player.ship.pos.x+player.ship.center.x)-(app.width/2) )
		offset.y = -( (player.ship.pos.y+player.ship.center.y)-(app.height/2) )
	end

	-- frames = frames + 1
	-- if(sys.time() - time > 1000) then
	-- 	print(frames, 'bullets '..bulletCount)
	-- 	--printClass(bullets)
	-- 	time = sys.time()
	-- 	frames = 0
	-- 	collectgarbage()
	-- end

	player:tick()
	if not player.ship.dead and player.ship.life <= 0 then
		for i = 1, 100 do
			table.insert(particles, Particle.new(Vec2.new(player.ship.pos.x, player.ship.pos.y)));
		end
		player.ship.dead = true
	end
	
	for i, ship in ipairs(spaceships) do
		ship:tick()
		if ship.ship.life <= 0 then
			for i = 1, 100 do
				table.insert(particles, Particle.new(Vec2.new(ship.ship.pos.x, ship.ship.pos.y)));
			end
			table.insert(effects, Explosion(Vec2(ship.ship.pos.x, ship.ship.pos.y)))
			table.insert(cargos, Cargo.new(ship.ship.pos))
			table.remove(spaceships, i)
		end

		-- spaceship collisions
		for v, othership in ipairs(spaceships) do
			if v ~= i then

				-- local dist = ship.pos:distance(othership.pos)
				-- if dist < ((ship.center.y*0.8)+(othership.center.y*0.8)) then
				-- 	ship.life = ship.life - (ship.speed:len()+othership.speed:len())
				-- 	local boosh = ship.pos:difference(othership.pos):normal()
				-- 	ship.speed:add( boosh:multipied(1):invert() )

				-- 	for k = 1, 1 do
				-- 		table.insert(particles, Particle.new( ship.pos:added( boosh:multipied((ship.pos:distance(othership.pos)/2)) ) ));
				-- 	end
				-- end

				ship.ship:testShipCollision(othership.ship)
			end
		end

		ship.ship:testShipCollision(player.ship)

		-- local dist = ship.pos:distance(player.ship.pos)
		-- if dist < ((ship.center.y*0.8)+(player.ship.center.y*0.8)) then
		-- 	ship.life = ship.life - (ship.speed:len()+player.ship.speed:len())
		-- 	local boosh = ship.pos:difference(player.ship.pos):normal()
		-- 	ship.speed:add( boosh:multipied(1):invert() )

		-- 	for k = 1, 1 do
		-- 		table.insert(particles, Particle.new( ship.pos:added( boosh:multipied((ship.pos:distance(player.ship.pos)/2)) ) ));
		-- 	end
		-- end
	end

	bulletCount = 0
	for i, bullet in ipairs(bullets) do
		bullet:tick()
		bulletCount = bulletCount + 1
		if bullet.dead then table.remove(bullets, i) end
	end

	particleCount = 0
	for i, particle in ipairs(particles) do
		particle:tick()
		particleCount = particleCount + 1
		if particle.life < 1 then table.remove(particles, i) end
	end
	for i, rocket in ipairs(rockets) do
		rocket:tick()

		for v, ship in ipairs(spaceships) do
			if rocket.pos:distance(ship.ship.pos) < ship.ship.center.y then
				ship.ship.life = ship.ship.life - 100
				table.remove(rockets, i)
				break
			end
		end
	end
	for i, cargo in ipairs(cargos) do
		cargo:tick()
	end
	for i, effect in ipairs(effects) do
		effect:tick()
	end

	ast:tick()
end

function simplify (num)

	return math.floor(num*1000)/1000
end