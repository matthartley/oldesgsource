Font = class({
	characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890  ' .. '!"$%*()-_+={}[]:;~#,.?/',
	size = 12.0,
	lineHeight = nil
})

function Font:construct (size)

	self.size = size
	self.lineHeight = self.size * 1.5
end

function Font:render (string, x, y)

	for i = 1, #string do

		local char = (string:sub(i, i))
		local s, e = string.find(self.characters, char, 1, true)
		--print(char, s)
		matrix.scale(self.size/8.0, self.size/8.0, 1)
		SpriteBuffer.render(fontsprites[s], shaders.sprite.shader, x + ((i-1)*self.size), y, 0, 0)
		matrix.identity()
	end
	--print(string.find(self.characters, string, 1, true))
end